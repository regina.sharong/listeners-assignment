import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class LoginTest extends BaseClass{

    LoginAction loginAction=new LoginAction();
    @Test
    public void loginTest() throws InterruptedException{
        eventFiringWebDriver.navigate().refresh();
        loginAction.clickSignUpButton(eventFiringWebDriver);
        //loginAction.getTextUsername(eventFiringWebDriver);
        //loginAction.clickUsername(eventFiringWebDriver);
        loginAction.fillSignUpDetails(eventFiringWebDriver,"Reginaaaaa","233846dkjc");
        loginAction.clickSignUp(eventFiringWebDriver);
        WebDriverWait wait= new WebDriverWait(eventFiringWebDriver,20);
        wait.until(ExpectedConditions.alertIsPresent());
        eventFiringWebDriver.switchTo().alert().accept();
        eventFiringWebDriver.navigate().refresh();
    }
}
