import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

import java.io.File;
import java.io.IOException;

public class MyTestListener extends AbstractWebDriverEventListener {
    @Override
    public void beforeAlertAccept(WebDriver driver) {
        System.out.println("Before accepting the alert window");
    }
    @Override
    public void afterAlertAccept(WebDriver driver) {
        System.out.println("After accepting the alert window");
    }
    @Override
    public void afterAlertDismiss(WebDriver driver) {
    }
    @Override
    public void beforeAlertDismiss(WebDriver driver) {
    }
    @Override
    public void beforeNavigateTo(String url, WebDriver driver) {
        System.out.println("Before Opening "+url);
    }
    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
        System.out.println("After Opening "+url+" in browser and opening "+driver.getTitle());
    }
    @Override
    public void beforeNavigateRefresh(WebDriver driver) {
        System.out.println("Before refreshing");
    }
    @Override
    public void afterNavigateRefresh(WebDriver driver) {
        System.out.println("After refreshing");
    }
    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("Before Finding "+driver.findElement(by).getAttribute("id"));
    }
    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("After Finding "+driver.findElement(by).getAttribute("id"));
    }
    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        System.out.println("beforeClicking on " +element.getText());
    }
    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        System.out.println("afterClicking on " +element.getText());
    }
    @Override
    public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        System.out.println("Entered "+element.getAttribute("value"));
    }

    @Override
    public void onException(Throwable throwable, WebDriver driver) {
        File file =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(file,new File("/Users/reginasharon/NeevProject/ListenersAssignment/src/main/screenshot.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public <X> void beforeGetScreenshotAs(OutputType<X> target) {
    }
    @Override
    public <X> void afterGetScreenshotAs(OutputType<X> target, X screenshot) {
    }
}

