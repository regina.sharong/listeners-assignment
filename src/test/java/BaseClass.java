import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class BaseClass {
        public WebDriver driver;
        public EventFiringWebDriver eventFiringWebDriver;


        @BeforeMethod
        public void setUp(){
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            eventFiringWebDriver =new EventFiringWebDriver(driver);
            MyTestListener myTestListener =new MyTestListener();
            eventFiringWebDriver.register(myTestListener);
            eventFiringWebDriver.manage().window().maximize();
            eventFiringWebDriver.get("https://www.demoblaze.com/");
            eventFiringWebDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        }

        @AfterMethod
        public void closeDriver(){
            driver.close();
        }




    }

