import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class LoginAction extends LoginObject{

    public void clickSignUpButton(EventFiringWebDriver eventFiringWebDriver) { eventFiringWebDriver.findElement(signUpButton).click();}

    public void getTextUsername(EventFiringWebDriver eventFiringWebDriver) { eventFiringWebDriver.findElement(usernameTitle).click();}

    public void clickUsername(EventFiringWebDriver eventFiringWebDriver) { eventFiringWebDriver.findElement(usernameTitle).click();}

    public void fillSignUpDetails(WebDriver driver, String username, String password) {
        driver.findElement(usernameObj).sendKeys(username);
        driver.findElement(passwordObj).sendKeys(password);
    }

    public void clickSignUp(EventFiringWebDriver eventFiringWebDriver) { eventFiringWebDriver.findElement(signUp).click();}
}
