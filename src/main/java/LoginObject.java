import org.openqa.selenium.By;
import org.testng.internal.BaseClassFinder;

public class LoginObject {
    public By signUpButton = By.id("signin2");
    public By usernameTitle = By.xpath("/html/body/div[2]/div/div/div[2]/form/div[1]/label");
    //public By signUpHeading = By.id("signInModalLabel");
    public By usernameObj = By.id("sign-username");
    public By passwordObj = By.id("sign-password");
    public By signUp = By.xpath("/html/body/div[2]/div/div/div[3]/button[2]");
}
